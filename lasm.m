% x^2 就是 x的平方 下面用的上





% int32（）的意思是将数据向上取整整数，这里面的数据是成对的关系（下面简称“关系”），
% 里面每行是一个关系，每行的两个数分别是在position表格中的点的序号，例如某一行是（1, 2）就是position中的第一行数据表示的点
% 和第二行数据表示的点 ，这个的意思应该是表示序号为1的点和序号为2的点的某种关系
DATA = int32(xlsread('bus685.xlsx',1,'A1:B120'))+1;

% 这个文件里面每一行都是一个点，每一行的两个数分别是这个点的x坐标和y坐标
position =xlsread('position.xlsx',1,'A1:B685');
 
% 不知道这些定义有什么意思，下面都用到了，应该是弹簧算法的参数
K_r = 0.1;
K_s = 0.025;
L = 2;
delta_t = 10;
MaxLength =8;
position_count = 100;

% tic的意思是记录程序开始的时间， toc是记录程序结束的时间，这两个结合就会在输出的地方显示这两个代码中间的
% 代码运行用了多长时间，和算法本身没关系
tic

% 就是下面这些操作重复一百遍的意思，应该是弹簧一百次的意思
for k = 1:100
%     force 为 685 * 2 矩阵
    force = zeros(position_count,2);
    
%     下面这两个嵌套的for循环应该是一起的， 是第一次变动force数据的操作
%     2 到 684 循环
    for m = 2:position_count - 1
%         m + 1 到 685 循环
        for n = m+1:position_count
            dx = position(n,1)-position(m,1);
            dy = position(n,2)-position(m,2);
%             ~= 为不等于
            if dx~=0 || dy~=0
                dist2 = dx^2+dy^2;
%                 dist 为m点到n点的直线距离
                dist = sqrt(dist2);
                
                f = K_r/dist2;
%                 f = f = K_r / (dist^2);
%                 fx = K_r * dx / (dist^3)
%                 fy = K_r * dy / (dist^3)
                fx = f*dx/dist;
                fy = f*dy/dist;
%                 看不懂
%                 force是个二维数组，每一行的两个数代表一个点的 x 方向 和 y
%                 方向的某些值，看不懂这是什么值，下面注释了这个值得计算方法。

%                   force（m, 1）应该就是第m行的第一个值，可能和x坐标上的计算有关
%                   force（m, 2）应该就是第m行的第一个值，可能和y坐标上的计算有关

%                 基本上就是685个点每个点都有一个序号，就是它在第几行

%                 这个二重循环的关键操作应该就是下面这四行
%                 大概总体的意思应该是 为每一个点加上这个点和它之前序号的点计算出来的fx与fy， 减去和它之后序号的点计算出来的fx和fy
                force(m,1) = force(m,1)-fx;
                force(m,2) = force(m,2)-fy;
                force(n,1) = force(n,1)+fx;
                force(n,2) = force(n,2)+fy;
            end
        end
    end
%     从第二个点到最后一个点
    for m = 2:position_count
        
%         取出所有序号为 m 的点（并且这个点在关系中的第一个 例如某一行的 （a, b）中a的位置 ）的 关联关系
        ne=find(DATA(:,1)==m);
%         用 for 循环遍历这些关联关系
        for n = 1:length(ne)
%            DATA(ne(n),2) 关系中的第二个点的序号
%            dx 为关系中第二个点 与 原点（序号为m的点）在x坐标上的差
            dx = position(DATA(ne(n),2),1)-position(m,1);
%            dy 为关系中第二个点 与 原点（序号为m的点）在y坐标上的差
            dy = position(DATA(ne(n),2),2)-position(m,2);
%            这句话是如果两个点在x坐标上相差不为0 或 这句话是如果两个点在x坐标上相差不为0 也就是如果这两个点不是同一个点
            if dx~=0||dy~=0
%                dist是这两个点的直线距离
                dist = sqrt(dx^2+dy^2);
%                 不懂为什么要减L ，应该是弹簧概念上的问题
                f = K_s*(dist-L);
%                 fx = K_s * dx * （dist - L） / dist  不懂为什么这么做
                fx = f*dx/dist;
%                 fy = K_s * dy * （dist - L） / dist  不懂为什么这么做
                fy = f*dy/dist;
%                 force二维数组中序号为m的点加上这个fx fy
                force(m,1) = force(m,1)+fx;
                force(m,2) = force(m,2)+fy;
%                 序号为 关系中的第二个点的 减去这个 fx fy 不知道为什么，可能是弹簧算法里面的
                force(DATA(ne(n),2),1) = force(DATA(ne(n),2),1)-fx;
                force(DATA(ne(n),2),2) = force(DATA(ne(n),2),2)-fy;

            end
        end
    end
%     从第二个点到第685个点的循环
    for i = 2:position_count
%         i为点的序号 
%         force（i, 1）就是上面计算出来的和序号为i的点的x坐标相关的值， 不知道为什么这么计算，这个值有什么用也不知道
        dx = delta_t*force(i,1);
%         force（i, 2）就是上面计算出来的和序号为i的点的y坐标相关的值， 不知道为什么这么计算，这个值有什么用也不知道
        dy = delta_t*force(i,2);
%         displacement就是根据这个fx和fy计算出来的
        displacement = dx^2+dy^2;
%         如果displacement 大于上面定义的MaxLength
        if(displacement>MaxLength)
%             s就等于 MaxLength除以displacement 再开平方
            s = sqrt(MaxLength/displacement);
%             这里的作用应该是让 dx^2 + dy^2 超过了Maxlength的 都小于这个MaxLength， 主要要的是 dx
%             和 dy
            dx = s*dx;
            dy = s*dy;
        end
%         这个循环的关键位置是下面两行，这个坐标点的x值加了dx，y值加了dy，应该就是弹簧导致了坐标点变动的意思
        position(i,1) = position(i,1)+dx;
        position(i,2) = position(i,2)+dy;
    end
end

% 程序结束的时间，和tic相对
toc


% 就是读取这1282条关系
for i = 1:120
    
%     某一条关系中第一个点的序号
    s = DATA(i,1);
%     某一条关系中第二个点的序号
    d = DATA(i,2);
%     把这两个点的x值组成一个数组
    x = [position(s,1);position(d,1)];
%     把这两个点的y值组成一个数组
    y = [position(s,2);position(d,2)];
%     这个是按照 x数组 和 y数组画点。配合上面两行，总的意思是画出这条关系中的两个点
%     'o-b'的意思 'o'是空心圆 '-'是实线 'b'是蓝色
     plot(x,y,'ob');
    
%    hold on 的意思是在每次新调用plot（）画图时，之前调用plot（）画的点不消失
     hold on;
    if(x(1) ~= y(1) && x(2) ~= y(2))
       plot(x(1),y(2),'*b')
       hold on;
       line1 = zeros(2);
       line1(1) = x(1);
       line1(2) = x(1);
       line2 = zeros(2);
       line2(1) = y(1);
       line2(2) = y(2);
       plot(line1, line2, '-b')
       line3 = zeros(2);
       line3(1) = x(1);
       line3(2) = x(2);
       line4 = zeros(2);
       line4(1) = y(2);
       line4(2) = y(2);
       plot(line3, line4, '-b')
    else
        plot(x,y,'-b')
    end
end

% 下面代码的意思是先定义sum 为 0，然后加上force数组中的每一个记录的绝对值，(包括 x坐标系相关的那个 和 y坐标系相关的那个)
sum = 0;
for j = 1:position_count
    sum = sum+abs(force(j,1))+abs(force(j,2));
end
% 输出sum
sum

% 现在是2:06 刚写完

